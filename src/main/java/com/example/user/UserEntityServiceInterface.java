package com.example.user;


import java.util.List;



public interface UserEntityServiceInterface {
    
    public UserEntity getUserById(Long Id);
    
    public UserEntity addUser(UserEntity user);
    
    public UserEntity updateUserById(String Newname, Long Id);
        
    public boolean deleteUserById(Long Id);
    
    public List<UserEntity> getAllUsers(); 
    
}
