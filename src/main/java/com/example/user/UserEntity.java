package com.example.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * Entity class representing table "users"
 */
@Entity
@Table(name = "users")
public class UserEntity implements Serializable {

    
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    
    /**
     * Users ID
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    
    /**
     * Users name
     */
    private String name;
    
    
    /**
     * Users password
     */ 
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;
    
    
    /**
     * Users username
     */ 
    @JsonProperty(access = Access.WRITE_ONLY)
    private String username;
    
    
    
    public UserEntity() {
    }

    
    /**
     * 
     * @param name Name of the user.
     * @param username Username of the user.
     * @param password Password of the user.
     */
    public UserEntity(String name, String username, String password) {
        this.name = name;
        this.username = username;
        setPassword(password);
    }
    
    
    
    
    /**
     * 
     * @return Users name
     */
    public String getName() {
        return name;
    }
 
    /**
     * 
     * @return User's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @return User's username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @return User's ID
     */
    public Long getId() {
        return id;
    }

    
    
    /**
     * Sets users name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Sets users password
     * @param password 
     */
    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }
    
    


}