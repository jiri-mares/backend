package com.example.user;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




@Service
public class UserEntityService implements UserEntityServiceInterface {
    
    
    @Autowired
    private UserRepository userRepository;
    
    
    
    
    /**
     * Finds user with given id
     * @param Id of the user
     * @return entity of the user
     */
    @Override
    public UserEntity getUserById(Long Id) {
        return userRepository.getUserById(Id);
    }

    
    
    /**
     * Adds new user with given name and password
     * @param user UserEntity containing new name, username and password
     * @return entity of the new user or null if that user already exists
     */
    @Override
    public UserEntity addUser(UserEntity user) {
        
        if(userRepository.existsByUsername(user.getUsername())){
            return null;            
        }        
              
        return userRepository.save(user);
    }
    
    
    
    /**
     * Updates name of a user with given Id
     * @param Newname New name for user with given Id
     * @param Id of user which is updated
     * @return updated UserEntity
     */
    @Override
    public UserEntity updateUserById(String Newname, Long Id){        
        
        UserEntity user = userRepository.getUserById(Id);
        
        if(user == null){
            return null;            
        }        
        
        user.setName(Newname);
        userRepository.save(user);        
                
        return user;
    }
    
    
    
    /**
     * Deletes user with given id
     * @param Id of user to delete
     * @return true if successful
     */
    @Override
    public boolean deleteUserById(Long Id){
        
        if(!userRepository.existsById(Id)){
            return false;            
        }
        
        userRepository.deleteById((long)Id);
                
        return true;
    }
    
    
    
    
    /**
     * 
     * @return List of all users in the database
     */
    @Override
    public List<UserEntity> getAllUsers() {        
        List<UserEntity> allUsers = new ArrayList<>();
        userRepository.findAll().forEach(allUsers::add);
        return allUsers;
    }

    
    
    
    

}