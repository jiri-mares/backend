package com.example.user;
 
 
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
 
 
@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
 
    UserEntity getUserById(Long Id);
    UserEntity getUserByName(String name);
    UserEntity getUserByUsername(String username);
    boolean existsByUsername(String username);
    @Override
    boolean existsById(Long Id);
    void deleteUserById(Long Id);
}

