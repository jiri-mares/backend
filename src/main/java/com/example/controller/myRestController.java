package com.example.controller;


import com.example.user.UserEntity;
import com.example.user.UserEntityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class myRestController {
    
    
    
    
    
    @Autowired
    private UserEntityService UserService;

    
    
    /**
     * Returns data from user 'id'
     * @param id of the user
     * @return 
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getid")
    public UserEntity getUserById(@RequestParam("id") String id) {  
        return UserService.getUserById(Long.parseLong(id));        
    }
    
    
    

    /**
     * Adds a new user
     * @param user UserEntity containing new name, new username and new password
     * @return 
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/addUser")
    public ResponseEntity addUser(@RequestBody UserEntity user/*@RequestParam String Name, @RequestParam String username, @RequestParam String password*/) {
               
        UserEntity newUser = UserService.addUser(user);
        
        if(newUser == null){
            return ResponseEntity.badRequest().body("Username already exists");
        }
        return ResponseEntity.ok(newUser);
    }
        
    
    
    /**
     * Updates user
     * @param user UserEntity containing Id and new name
     * @return 
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.PUT, value = "/updateUser")
    public ResponseEntity updateUserById(@RequestBody UserEntity user) {
                
        UserEntity newUser = UserService.updateUserById(user.getName(), user.getId());
        
        if(newUser == null){
            return ResponseEntity.badRequest().body("ID does not exists");
        }
        return ResponseEntity.ok(newUser);
    }
    
        
    
    
    /**
     * Lists all users
     * @return 
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/list")
    public List<UserEntity> ListAllUsers() {
        return UserService.getAllUsers();
    }
    
    
    
    
}