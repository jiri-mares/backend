package com.example.controller;


import com.example.user.UserEntity;
import com.example.user.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecuredRestController {        
    
    
    
    @Autowired
    private UserEntityService UserService;
    
    /**
     * Deletes user with given Id
     * @param user UserEntity containing Id
     * @return 
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, value = "/secured/deleteUser")
    public boolean deleteUserById(@RequestBody UserEntity user) {  
        return UserService.deleteUserById(user.getId());        
    }

    
    
}